<?php


Route::group(['prefix' => 'v1'], function () {
    Route::post('/login', 'Api\\v1\\AuthController@login');
    Route::get('/user', 'Api\\v1\\AuthController@profile')->middleware('multiauth:admin');
});
