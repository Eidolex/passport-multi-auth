<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v2'], function () {

    Route::middleware('multiauth:api')->get('/user', 'Api\\v2\\AuthController@profile');

    Route::post('/login', 'Api\\v2\\AuthController@login');
});
