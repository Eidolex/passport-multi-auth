<?php

namespace App\Http\Controllers\Api\v2;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\User;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials)) {
            // Authentication passed...
            $user = Auth::user();
            if ($user instanceof User) {
                $token = $user->createToken(null);
                return response()->json(['token' => $token->accessToken]);
            }
            // return redirect()->intended('dashboard');
        } else {
            return 'false';
        }
    }

    public function profile(Request $request)
    {
        return response()->json($request->user());
    }
}
