<?php

namespace App\Http\Controllers\Api\v1;

use App\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');
        if (Auth::guard('admin-guard')->attempt($credentials)) {
            // Authentication passed...
            $admin = Auth::guard('admin-guard')->user();
            if ($admin instanceof Admin) {
                $token = $admin->createToken(null);
                return response()->json(['token' => $token->accessToken]);
            }
            // return redirect()->intended('dashboard');
        } else {
            return 'false';
        }
    }

    public function profile(Request $request)
    {
        return response()->json($request->user());
    }
}
