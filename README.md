## Compatibility

| Laravel Passport |
| ---------------- |
| ^5.0             |
| ^6.0             |
| ^7.0             |

| Laravel Framework | Passport Multiauth |
| ----------------- | ------------------ |
| <= 5.6            | <= 3.0             |
| 5.7.x             | 4.0.x              |

## Installing and configuring

Install using composer:

```sh
$ composer require smartins/passport-multiauth
```

If you are using a Laravel version **less than 5.5** you **need to add** the provider on `config/app.php`:

```php
    'providers' => [
        // ...
        SMartins\PassportMultiauth\Providers\MultiauthServiceProvider::class,
    ],
```

Migrate database to create `oauth_access_token_providers` table:

```sh
$ php artisan migrate
```

Instead of using the `Laravel\Passport\HasApiTokens` trait from [Laravel Passport](https://laravel.com/docs/5.6/passport#installation) core, use the trait `SMartins\PassportMultiauth\HasMultiAuthApiTokens` provided by this package.
Internally, this `HasMultiAuthApiTokens` uses the `HasApiTokens`, overriding the methods `tokens()` and `createToken($name, $scopes = [])`.
The behavior of the method `tokens()` was changed to join with the table `oauth_access_token_providers` getting just the tokens created
to specific model.
The method `createToken($name, $scopes = [])` was changed to create the token using the `provider` defined to model on `config/auth.php`.
Now when you create the token, this token will be related with the model that is calling.

Add new provider in `config/auth.php` using a model that extends of `Authenticatable` class and use `HasMultiAuthApiTokens` trait.

Example:

Configure your model:

```php

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use SMartins\PassportMultiauth\HasMultiAuthApiTokens;

class Admin extends Authenticatable
{
    use Notifiable, HasMultiAuthApiTokens;
}

```

And your `config/auth.php` providers:

```php
    // ...

    'providers' => [
        'users' => [
            'driver' => 'eloquent',
            'model' => App\User::class,
        ],

        // ** New provider**
        'admins' => [
            'driver' => 'eloquent',
            'model' => App\Admin::class,
        ],
    ],

    // ...
```

Add a new `guard` in `config/auth.php` guards array using driver `passport` and the provider added above:

```php
    // ...

    'guards' => [
        'web' => [
            'driver' => 'session',
            'provider' => 'users',
        ],

        'api' => [
            'driver' => 'passport',
            'provider' => 'users',
        ],

        // ** New guard **
        'admin' => [
            'driver' => 'passport',
            'provider' => 'admins',
        ],
    ],

    // ...
```

Register the middleware `AddCustomProvider` to `$routeMiddleware` attributes on `app/Http/Kernel.php` file.

```php

class Kernel extends HttpKernel
{
    // ...

    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually.
     *
     * @var array
     */
    protected $routeMiddleware = [
        // ...
        'oauth.providers' => \SMartins\PassportMultiauth\Http\Middleware\AddCustomProvider::class,
    ];

    // ...
}
```

**OBS:** The param `provider` is required to routes wrapped by `AddCustomProvider` middleware.
You must to pass a valid provider configured on `config/auth.php`.

Add new middleware `Authenticate` on `app/Http/Kernel` `$routeMiddleware` attribute.

```php

class Kernel extends HttpKernel
{
    // ...

    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth' => \Illuminate\Auth\Middleware\Authenticate::class,
        // ** New middleware **
        'multiauth' => \SMartins\PassportMultiauth\Http\Middleware\MultiAuthenticate::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'bindings' => \Illuminate\Routing\Middleware\SubstituteBindings::class,
        'can' => \Illuminate\Auth\Middleware\Authorize::class,
        'guest' => \App\Http\Middleware\RedirectIfAuthenticated::class,
        'oauth.providers' => \SMartins\PassportMultiauth\Http\Middleware\AddCustomProvider::class,
        'throttle' => \Illuminate\Routing\Middleware\ThrottleRequests::class,
    ];

    // ...
}
```
